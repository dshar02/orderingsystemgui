/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordersystem;
import java.io.*;
/**
 *
 * @author david
 */
public class order implements Serializable
{
private boolean isDouble;
private boolean hasFries;
private boolean hasDrink;
private menuItem menuItem;
private double orderTotal;
private int orderNumber;



    public order(boolean isDouble, boolean hasFries, boolean hasDrink, menuItem menuItem, double orderTotal, int orderNumber) {
        this.isDouble = isDouble;
        this.hasFries = hasFries;
        this.hasDrink = hasDrink;
        this.menuItem = menuItem;
        this.orderTotal = orderTotal;
        this.orderNumber = orderNumber;
    }
    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public boolean isIsDouble() {
        return isDouble;
    }

    public void setIsDouble(boolean isDouble) {
        this.isDouble = isDouble;
    }

    public boolean isHasFries() {
        return hasFries;
    }

    public void setHasFries(boolean hasFries) {
        this.hasFries = hasFries;
    }

    public boolean isHasDrink() {
        return hasDrink;
    }

    public void setHasDrink(boolean hasDrink) {
        this.hasDrink = hasDrink;
    }

    public menuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(menuItem menuItem) {
        this.menuItem = menuItem;
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }
    
}
