/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordersystem;
import java.io.*;
/**
 *
 * @author david
 */
public class menuItem implements Serializable
{
private String itemName;
private double itemPrice;

    public menuItem(String itemName, double itemPrice) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }

    public boolean equals(menuItem menuItem)
    {
    return(this.itemPrice == menuItem.itemPrice && 
            this.itemName.equals(menuItem.itemName));
    }
    
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }
    
}
