/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordersystem;
import javax.swing.*;
import java.util.*;
import java.io.*;
/**
 *
 * @author david
 */
public class newMenuItem extends javax.swing.JFrame {

    ArrayList<menuItem> menuItems;
    /**
     * Creates new form newMenuItem
     */
    public newMenuItem() {
        initComponents();
        menuItems = new ArrayList<menuItem>();
        populateArrayList();
    }
    
    public void populateArrayList()
    {
    try
    {
        FileInputStream file = new FileInputStream("menuItems.dat");
        ObjectInputStream inputFile = new ObjectInputStream(file);
        boolean endOfFile = false;
        while(!endOfFile)
        {
            try
            {
               menuItems.add((menuItem)inputFile.readObject()); 
            }
            catch(EOFException e)
            {
            endOfFile = true;    
            }
            catch(Exception f)
            {
            JOptionPane.showMessageDialog(null, f.getMessage());    
            }
        }
        inputFile.close();
    }
    catch(IOException e){
    JOptionPane.showMessageDialog(null, e.getMessage());
    }
    }
    public void saveMenuItemsToFile()
    {
    try
    {
    FileOutputStream file = new FileOutputStream("menuItems.dat");
    ObjectOutputStream outputFile = new ObjectOutputStream(file);
    
    for(int i = 0; i< menuItems.size();i++){
    outputFile.writeObject(menuItems.get(i));
    }
    outputFile.close();
    JOptionPane.showMessageDialog(null, "New item created");
    newMenuItem.this.setVisible(false);
    }
    catch(IOException e)
    {
    JOptionPane.showMessageDialog(null, e.getMessage());
    }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        newItemScreenLabel = new javax.swing.JLabel();
        newItemNameLabel = new javax.swing.JLabel();
        newItemPriceLabel = new javax.swing.JLabel();
        newItemNameText = new javax.swing.JTextField();
        newItemPriceText = new javax.swing.JTextField();
        newMenuItemSaveButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("New Menu Item");

        newItemScreenLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        newItemScreenLabel.setText("Create a new menu item:");

        newItemNameLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        newItemNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        newItemNameLabel.setText("Item Name:");

        newItemPriceLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        newItemPriceLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        newItemPriceLabel.setText("Item Price:");

        newItemNameText.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        newItemPriceText.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        newMenuItemSaveButton.setText("Save");
        newMenuItemSaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemSaveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(newItemPriceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(newItemNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(newItemNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(newItemPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(newItemScreenLabel)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(207, 207, 207)
                        .addComponent(newMenuItemSaveButton)))
                .addContainerGap(85, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(newItemScreenLabel)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newItemNameLabel)
                    .addComponent(newItemNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newItemPriceLabel)
                    .addComponent(newItemPriceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(newMenuItemSaveButton)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void newMenuItemSaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newMenuItemSaveButtonActionPerformed
        // TODO add your handling code here:
      if(newItemNameText.getText().isEmpty() || newItemPriceText.getText().isEmpty() ){
      JOptionPane.showMessageDialog(null, "Please enter all fields");
      }
      else
      {  
      String name = newItemNameText.getText().trim();
      String price = newItemPriceText.getText().trim();
      
      menuItem menuIt = new menuItem(name,Double.parseDouble(price));
      menuItems.add(menuIt);
      saveMenuItemsToFile();
      }
    }//GEN-LAST:event_newMenuItemSaveButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(newMenuItem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(newMenuItem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(newMenuItem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(newMenuItem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new newMenuItem().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel newItemNameLabel;
    private javax.swing.JTextField newItemNameText;
    private javax.swing.JLabel newItemPriceLabel;
    private javax.swing.JTextField newItemPriceText;
    private javax.swing.JLabel newItemScreenLabel;
    private javax.swing.JButton newMenuItemSaveButton;
    // End of variables declaration//GEN-END:variables
}
